import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
   public users: any;
   public image: any = 'http://localhost:2300/';
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getuser().subscribe((data: any) => {
      this.users = data.product;
      console.log(this.users);
      console.log(this.users[0].products.product_name);
    });
  }

}
